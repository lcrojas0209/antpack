package routes

import (
	"Estudio/antpack/controller"
	"Estudio/antpack/middlewares"
	"github.com/gin-gonic/gin"
)

func InitRoutes() *gin.Engine  {
	r := gin.New()

	Routes(r)

	return r
}

func Routes(r *gin.Engine)  {
	v1 := r.Group("v1")
	{
		user := v1.Group("users")
		user.POST("/register", controller.RegisterUser)
		user.POST("/login", controller.LoginUser)

		v1.Use(middlewares.AuthHandlerFunc(""))
		{
			list := v1.Group("listUsers")
			list.GET("/users/:status",controller.GetUsers)
		}
	}
}
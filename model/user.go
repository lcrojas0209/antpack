package model

import (
	"Estudio/antpack/shared"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name     string `json:"name" db:"name"`
	Email    string `json:"email" db:"email" gorm:"UNIQUE_INDEX;NOT NULL"`
	Password string `json:"-" db:"password" gorm:"not null"`
	Status   int `json:"status" db:"status"`
}

type UserTokenSession struct {
	gorm.Model
	UserID   int64  `json:"user_id" db:"user_id"`
	Token    string `json:"token" db:"token"`
	Activate bool   `json:"activate" db:"activate" gorm:"default: true"`
}

type UserRegister struct {
	Name     string `json:"name"  binding:"required"`
	Email    string `json:"email"  binding:"required"`
	Password string `json:"password"  binding:"required"`
}

type UserLogin struct {
	Email    string `json:"email"  binding:"required"`
	Password string `json:"password"  binding:"required"`
}

type ResponseLogin struct {
	Token   string `json:"token"`
	User    *User  `json:"user"`
	Code    int    `json:"code"`
	Logged  bool   `json:"logged"`
}

func CreateUserModel(userRequest UserRegister) (User, error) {
	user := User{}

	result := shared.GetDb().Where("email = ?",userRequest.Email).Find(&user)
	if result.RowsAffected > 0  {
		return user,errors.New("El email ya existe en la base de datos.")
	}

	user.Name = userRequest.Name
	user.Email = userRequest.Email
	user.Password = EncryptPass(userRequest.Password)
	user.Status = 1

	err := shared.GetDb().Create(&user).Error
	if err != nil {
		return user, errors.New("Error al registrar tu usuario")
	}
	return user, nil
}

func EncryptPass(pass string) string  {
	password := sha256.Sum256([]byte(pass))
	encrypt := base64.StdEncoding.EncodeToString(password[:])
	return encrypt
}

func GetUser(email string) (*User,error) {
	var user *User

	value := shared.GetDb().Where("email=?", email).Find(&user)
	if value.RowsAffected == 0  {
		return user,errors.New("Usuario no existe.")
	}
	return user, nil
}

func SavedUserToken(userID uint, token string) error {
	userToken := UserTokenSession{}
	userToken.Token = token
	userToken.UserID = int64(userID)
	userToken.Activate = true

	err := shared.GetDb().Create(&userToken).Error

	return err
}

func GetUsersModel(status int64) ([]User, error) {
	var users []User

	err :=  shared.GetDb().Where("status=?", status).Find(&users).Error
	if err != nil {
		return users,err
	}

	return users,nil
}
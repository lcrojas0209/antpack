package controller

import (
	"Estudio/antpack/model"
	"Estudio/antpack/services"
	"Estudio/antpack/shared"
	"github.com/gin-gonic/gin"
	"strconv"
)

func RegisterUser(c *gin.Context)  {
	response := shared.Context{}
	userRequest := model.UserRegister{}

	if err := c.ShouldBind(&userRequest); err != nil  {
		c.JSON(401, response.ResponseData(shared.StatusFail,err.Error(),nil))
		return
	}

	user, err := services.CreateUser(userRequest)
	if err != nil {
		c.JSON(shared.StatusBad, response.ResponseData(shared.StatusFail, err.Error(), ""))
	} else {
		c.JSON(shared.StatusOk, response.ResponseData(shared.StatusSuccess,"",user))
	}
}

func LoginUser(c *gin.Context)  {
	response := shared.Context{}
	LoginRequest := model.UserLogin{}

	if err := c.ShouldBind(&LoginRequest); err != nil  {
		c.JSON(401, response.ResponseData(shared.StatusFail,err.Error(),nil))
		return
	}

	user, err := services.LoginUser(LoginRequest)
	if user.Logged == true && err == nil {
		c.JSON(shared.StatusOk, response.ResponseData(shared.StatusSuccess,"",user))
	} else {
		c.JSON(user.Code, response.ResponseData(shared.StatusFail,err.Error(),nil))
	}
}

func GetUsers(c *gin.Context)  {
	status,_ := strconv.ParseInt(c.Param("status"),10,64)
	response := shared.Context{}

	users, err := services.GetUsersServices(status)
	if err != nil {
		c.JSON(shared.StatusBad, response.ResponseData(shared.StatusFail, err.Error(), ""))
	}
	c.JSON(shared.StatusOk,response.ResponseData(shared.StatusSuccess,"",users))
}
package main

import (
	"Estudio/antpack/routes"
	"Estudio/antpack/shared"
)

func main()  {
	r := routes.InitRoutes()

	shared.Init()
	defer shared.CloseDb()

	//shared.DB.AutoMigrate(&model.User{})
	//shared.DB.AutoMigrate(&model.UserTokenSession{})

	r.Run(":3000")
}

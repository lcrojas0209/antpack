# antPack

## Preparacion 

Hacer uso de:

`git clone https://gitlab.com/lcrojas0209/antpack.git`

Se debe posicionar el clone a: `$GOPATH/Estudio/antpack/`

En caso de ser necesario usar el siguiente comando para descargar paquetes utilizados:
```
go get -d -v ./...
go install -v ./...
```

# Ejecucion

Para ejecutarlo solo hay que estar en raíz y ejecutar el siguente comando:

`go run main.go`  

#BD

Se debe crear la BD antPack usar como referencia el archivo `query`

Indicar en el archivo .env:
```
usuario
password
DB name
```

Por ultimo desde el archivo `main.go` ejecutar las migraciones (quitar comentarios).
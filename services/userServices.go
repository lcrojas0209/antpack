package services

import (
	"Estudio/antpack/middlewares"
	"Estudio/antpack/model"
	"Estudio/antpack/shared"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
)

func CreateUser(userRequest model.UserRegister) (model.User, error) {
	users, err := model.CreateUserModel(userRequest)
	if err != nil {
		return users,err
	}
	return users, nil
}

func LoginUser(login model.UserLogin) (model.ResponseLogin,error) {
	passwordRecived := model.EncryptPass(login.Password)

	user, err := model.GetUser(login.Email)
	if err != nil {
		return model.ResponseLogin{Token:"",User:nil,Code: shared.StatusBad}, err
	}

	if passwordRecived == user.Password {
		fmt.Println(passwordRecived,user.Password)
		if user.Status == 0 {
			return model.ResponseLogin{
				Token: "",
				User:  nil,
				Code:  shared.StatusUnauthorized,
				Logged: false,
			}, errors.New("Usuario inactivo.")
		}
		token := jwt.New(jwt.SigningMethodHS256)

		claims := make(jwt.MapClaims)
		claims["user_id"] = user.ID
		claims["email"] = user.Email
		claims["exp"] = time.Now().Add(time.Hour*1)
		token.Claims = claims

		tokenString, err := token.SignedString([]byte(middlewares.SigningKey))
		errToken := model.SavedUserToken(user.ID,tokenString)
		if errToken != nil {
			return model.ResponseLogin{
				Token:  "",
				User:   nil,
				Code:   0,
				Logged: false,
			}, errToken
		}
		if err == nil {
			return model.ResponseLogin{
				Token: tokenString,
				User:  user,
				Code:  shared.StatusOk,
				Logged: true,
			},nil
		}
	}
	return model.ResponseLogin{
		Token: "",
		User:  nil,
		Code:  shared.StatusBad,
		Logged: false,
	} ,errors.New("Contraseña incorrecta")
}

func GetUsersServices(status int64) ([]model.User, error) {
	users, err := model.GetUsersModel(status)
	if err != nil {
		return users, err
	}
	return users, nil
}
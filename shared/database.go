package shared

import (
	"fmt"
	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

var DB *gorm.DB
var err error

func Init()  {

	e :=godotenv.Load(os.ExpandEnv(".env"))
	if e != nil {

	}

	DATABASE_DB := os.Getenv("DATABASE_DB")
	DATABASE_USER := os.Getenv("DATABASE_USER")
	DATABASE_PASS := os.Getenv("DATABASE_PASS")

	user := DATABASE_USER
	password := DATABASE_PASS
	name := DATABASE_DB

	//dbInfo := "host= localhost user=postgres password=LUIScamilo1 dbname=antPack port=5432 sslmode=disable"
	dbInfo := fmt.Sprintf("host= localhost user=%s password=%s dbname=%s port=5432 sslmode=disable",
		user,
		password,
		name,
		)


	DB, err = gorm.Open(postgres.Open(dbInfo) ,&gorm.Config{})
	if err != nil {
		panic(err)
	}
}

func GetDb() *gorm.DB {
	return DB
}

func CloseDb()  {
	sqlDB, _ := DB.DB()
	sqlDB.Close()
}


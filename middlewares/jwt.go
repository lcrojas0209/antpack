package middlewares

import (
	"Estudio/antpack/model"
	"Estudio/antpack/shared"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

var (
	SigningKey = "$antPack$"
)


func AuthHandlerFunc(authRoles ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")

		if len(token) < 1 {
			c.JSON(403,gin.H{"message": "No se envio el token de autorizacion", "status": shared.StatusError, "data": nil })
			c.Abort()
			return
		}

		val, err := Validate(token, SigningKey)
		if err != nil {
			c.JSON(403, gin.H{"message": "Token de autorizacion no valida","status": err.Error(), "data": nil })
			c.Abort()
			return
		}

		user :=model.User{}
		errS := shared.GetDb().Where("id = ? and status=1", val.Claims.(jwt.MapClaims)["user_id"]).First(&user).Error
		if errS != nil {
			c.JSON(403, gin.H{"message": "Invalidada token de autorizacion", "status": shared.StatusError, "data":nil})
			c.Abort()
			return
		} else {
			c.Set("user", user)
			c.Next()
		}
	}
}

func Validate(tokenValidated string, key string ) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenValidated, func(token *jwt.Token) (interface{}, error) {
		return []byte(key),nil
	})
	return token,err
}